/**
 * Kelsey Pereira
 * 2032587
 */
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class ConeTests {
    @Test
    public void contructorNegativeTest1(){
        boolean checker = false;
        try{
            Cone c1 = new Cone(-1,5);
        } catch (IllegalArgumentException exception){
            checker = true;
        }
        assertEquals(true, checker);
    }
    @Test
    public void contructorNegativeTest2(){
        boolean checker = false;
        try{
            Cone c1 = new Cone(1,-5);
        } catch (IllegalArgumentException exception){
            checker = true;
        }
        assertEquals(true, checker);
    }
    @Test
    public void contructorNegativeTest3(){
        boolean checker = false;
        try{
            Cone c1 = new Cone(-1,-5);
        } catch (IllegalArgumentException exception){
            checker = true;
        }
        assertEquals(true, checker);
    }
    @Test
    public void getVolumeTest1(){
        Cone c1 = new Cone(2,2);
        assertEquals(8.38, c1.getVolume(), 0.01);
    }
    @Test
    public void getVolumeTest2(){
        Cone c1 = new Cone(7,5);
        assertEquals(183.26, c1.getVolume(), 0.01);
    }
    @Test
    public void getSurfaceAreaTest1(){
        Cone c1 = new Cone(2,2);
        assertEquals(30.34, c1.getSurfaceArea(), 0.01);
    }
    @Test
    public void getSurfaceAreaTest2(){
        Cone c1 = new Cone(9,4);
        assertEquals(174.03, c1.getSurfaceArea(), 0.01);
    }
    @Test
    public void getRadiusTest(){
        Cone c1 = new Cone(2,3);
        assertEquals(2, c1.getHeight());
    }
    @Test
    public void getHeightTest(){
        Cone c1 = new Cone(2,3);
        assertEquals(3, c1.getRadius());
    }
}