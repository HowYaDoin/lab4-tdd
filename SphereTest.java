// Nicoleta Sarghi 2039804
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class SphereTest {
    @Test
    public void TestUnderZeroException(){
        boolean exceptionTriggered = false;
        try{
            double radius = -1;
            Sphere sphere = new Sphere(radius);
        }
        catch(IllegalArgumentException exception){
            exceptionTriggered = true;
        }
        assertEquals(true,exceptionTriggered);
    }
    @Test     
    public void TestGettersA(){
        double radius = 4.20;
        Sphere sphere = new Sphere(radius);
        assertEquals(radius,sphere.getRadius());
    }
    @Test
    public void TestGettersB(){
        double radius = 24.6;
        Sphere sphere = new Sphere(radius);
        assertEquals(radius,sphere.getRadius());
    }
    @Test     
    public void TestVolumeA(){
        double radius = 9.69;
        Sphere sphere = new Sphere(radius);
        assertEquals(3811.18421,sphere.getVolume(),0.00001);
    }
    @Test     
    public void TestVolumeB(){
        double radius = 3.867;
        Sphere sphere = new Sphere(radius);
        assertEquals(242.22063,sphere.getVolume(),0.00001);
    }
    @Test 
    public void TestSurfaceAreaA(){
        double radius = 10.35;
        Sphere sphere = new Sphere(radius);
        assertEquals(1346.14104,sphere.getSurfaceArea(),0.00001);        
    }        
    @Test 
    public void TestSurfaceAreaB(){
        double radius = 5.16;
        Sphere sphere = new Sphere(radius);
        assertEquals(334.58716,sphere.getSurfaceArea(),0.00001);        
    }   
}
