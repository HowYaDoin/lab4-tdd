// Nicoleta Sarghi 2039804
public class Cone{
    // A cone has a height (base to the tip) and a radius (radius of the circular base)
    // The constructor sets those properties
    double height;
    double radius;
    public Cone(double height, double radius){
        if(height<0){
            throw new IllegalArgumentException("Cone height is negative");
        }
        if(radius<0){
            throw new IllegalArgumentException("Cone radius is negative");
        }
        this.height = height;
        this.radius = radius;
    } 

    // getters for height and radius
    public double getHeight() {
        return height;
    }
    public double getRadius() {
        return radius;
    }

    // Volume and surface area
    // Calculates the volume using height and radius and returns it
    public double getVolume(){
        return (radius*radius*Math.PI)*height/3;
    }
    // Calculates the surface area using height and radius and returns it
    public double getSurfaceArea(){
        return Math.PI*radius*(radius+Math.sqrt(height*height+radius*radius));
    }
    

}