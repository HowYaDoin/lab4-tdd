/**
 * Kelsey Pereira
 * 2032587
 */
public class Sphere{
    private double radius;
    public Sphere(double r){
        if (r < 0){
            throw new IllegalArgumentException("Radius must be more then 0");
        }
        this.radius = r;
    }
    public double getRadius(){
        return this.radius;
    }
    public double getVolume(){
        return 4.0/3.0 * Math.PI * radius * radius * radius;
    }
    public double getSurfaceArea(){
        return 4 * Math.PI * radius * radius;
    }
}